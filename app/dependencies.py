import os

import psycopg2 as pg

class DBConnection:
    """Handles database connections as a context.

    Example usage:

    with DBConnection() as db_connection:
        db_connection.cursor.execute(...)
    """

    def __init__(self, connection_string=os.environ.get('CONNECTION_STRING', 'host=db dbname=pf user=postgres password=example')):
        """Initializes the connection string.

        Args:
            connection_string (string, optional): Should be passed as a environment variable named "CONNECTION_STRING".
            Defaults to 'host=db dbname=pf user=postgres password=example'.
        """
        self.connection_string = connection_string
        self.connection = None
        self.cursor = None

    def __enter__(self):
        """Context entrypoint. Connects to the database and sets up the cursor."""
        self.connection = pg.connect(self.connection_string)
        self.cursor = self.connection.cursor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Context exit point. Closes the connection and cursor. If operation was successful, it is commited.
        Otherwise, apply a rollback to the connection.

        Args:
            exc_type (_type_): The type of the exception.
            exc_val (_type_): The value of the exception.
            exc_tb (_type_): The traceback of the exception.
        """
        if exc_tb is None:
            self.connection.commit()
        else:
            self.connection.rollback()
        self.cursor.close()
        self.connection.close()
