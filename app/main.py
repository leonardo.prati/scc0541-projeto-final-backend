# Load login routes from backend/app/routers/login.py
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routers.login import router as login_router
from routers.driver import router as driver_router
from routers.constructor import router as constructor_router
from routers.season import router as season_router
from routers.race import router as race_router
from routers.status import router as status_router
from routers.airport import router as airport_router

# Create app
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

#Bind login routes
app.include_router(login_router, prefix="/login")
app.include_router(driver_router, prefix="/driver")
app.include_router(constructor_router, prefix="/constructor")
app.include_router(season_router, prefix="/season")
app.include_router(race_router, prefix="/race")
app.include_router(status_router, prefix="/status")
app.include_router(airport_router, prefix="/airport")

# Starts app in port 3000
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=3000)

