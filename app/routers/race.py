from fastapi import APIRouter

from dependencies import DBConnection

router = APIRouter()


@router.get("/count", tags=["races"], response_model=dict)
async def get_races_count():
    with DBConnection() as db_connection:
        db_connection.cursor.execute("SELECT COUNT(*) FROM races")
        count = db_connection.cursor.fetchone()
        return {'races_count': count[0]}
