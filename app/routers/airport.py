from fastapi import APIRouter
from dependencies import DBConnection

router = APIRouter()

@router.get("/{city_name}/distance", tags=["airport"])
async def get_airport_by_city_name(city_name: str, max_distance: int = 100):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT CityName, AirportIATACode, AirportName, AirportCity, EarthDistance, AirportType " \
            "FROM " \
            "( " \
            "    SELECT " \
            "        C.Name AS CityName, " \
            "        A.IATACode AS AirportIATACode, " \
            "        A.Name AS AirportName, " \
            "        A.city AS AirportCity, " \
            "        A.type AS AirportType, " \
            "        earth_distance ( " \
            "            ll_to_earth (A.Lat, A.Long), " \
            "            ll_to_earth (C.Lat, C.Long) " \
            "        ) AS EarthDistance " \
            "    FROM " \
            "        ( " \
            "            SELECT Name, LatDeg Lat, LongDeg Long, IATACode, city, type " \
            "            FROM " \
            "                Airports " \
            "            WHERE " \
            "                isocountry = 'BR' AND " \
            "                type = 'medium_airport' " \
            "        ) A, " \
            "        ( " \
            "            SELECT Name, Lat, Long Long " \
            "            FROM geocities15k " \
            "            WHERE " \
            "                name = %s " \
            "        ) C " \
            ") X " \
            "WHERE EarthDistance < %s;", (city_name, max_distance * 1000)
        )
        results = db_connection.cursor.fetchall()
        results = [
            {
                'Nome da Cidade': result[0],
                'Código IATA': result[1],
                'Nome do Aeroporto': result[2],
                'Cidade do Aeroporto': result[3],
                'Distância [km]': round(result[4]/1000, 2),
                'Tipo de Aeroporto': result[5]
            }
            for result in results
        ]
        return results