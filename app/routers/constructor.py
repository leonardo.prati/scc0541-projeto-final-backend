from typing import List, Union
from fastapi import APIRouter, Response

from dependencies import DBConnection
from models.constructor import Constructor, NewConstructor


router = APIRouter()


@router.get("/{constructor_id}", tags=["constructor"], response_model=Constructor)
async def get_constructor_by_id(constructor_id: int):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT * FROM constructors WHERE constructorId = %s", (
                constructor_id,)
        )
        constructor = db_connection.cursor.fetchone()
        if constructor:
            return Constructor(
                constructorId=constructor[0],
                constructorRef=constructor[1],
                name=constructor[2],
                nationality=constructor[3],
                url=constructor[4]
            )


@router.get("/{constructor_id}/wins", tags=["constructor"], response_model=dict)
async def get_constructor_wins(constructor_id: int):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT COUNT(*) FROM results WHERE constructorId = %s AND position = 1", (constructor_id,)
        )
        count = db_connection.cursor.fetchone()
        return {'wins': count[0]}


@router.get("/{constructor_id}/drivers", tags=["constructor"], response_model=dict)
async def get_constructor_number_of_drivers(constructor_id: int):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT COUNT(DISTINCT driverId) FROM results WHERE constructorId = %s", (constructor_id,)
        )
        count = db_connection.cursor.fetchone()
        return {'drivers': count[0]}


@router.get("/{constructor_id}/drivers/{forename}", tags=["constructor"])
async def search_driver_forname(constructor_id: str, forename: str):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT DISTINCT driver.forename, Driver.surname, driver.dob, driver.nationality FROM driver " \
            "JOIN results ON driver.driverId = results.driverId "
            "WHERE results.constructorId = %s AND " \
            "driver.forename = %s", (constructor_id, forename,)
        )
        drivers = db_connection.cursor.fetchall()
        return drivers


@router.get("/{constructor_id}/years", tags=["constructor"], response_model=dict)
async def get_constructor_years(constructor_id: int):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT MIN(year), MAX(year) FROM results "
            "JOIN races ON results.raceId = races.raceId "
            "WHERE constructorId = %s", (constructor_id,)
        )
        years = db_connection.cursor.fetchone()
        return {'first_year': years[0], "last_year": years[1]}


@router.get("/", tags=["constructor"], response_model=Union[List[Constructor], dict])
async def get_all_constructors(count_only: bool = False):
    if count_only:
        with DBConnection() as db_connection:
            db_connection.cursor.execute("SELECT COUNT(*) FROM constructors")
            count = db_connection.cursor.fetchone()
            return {'constructors_count': count[0]}
    with DBConnection() as db_connection:
        db_connection.cursor.execute("SELECT * FROM constructors")
        constructors = db_connection.cursor.fetchall()
        return [
            Constructor(
                constructorId=constructor[0],
                constructorRef=constructor[1],
                name=constructor[2],
                nationality=constructor[3],
                url=constructor[4]
            )
            for constructor in constructors
        ]


@router.post("/", tags=["constructor"])
async def create_constructor(constructor: NewConstructor):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "INSERT INTO constructors "
            "VALUES (DEFAULT, %s, %s, %s, %s)",
            (constructor.constructorRef, constructor.name,
             constructor.nationality, constructor.url)
        )
