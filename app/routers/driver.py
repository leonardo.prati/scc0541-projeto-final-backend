from lib2to3.pgen2 import driver
from typing import List, Union
from fastapi import APIRouter, Response

from dependencies import DBConnection
from models.driver import Driver, NewDriver

router = APIRouter()


@router.get("/{driver_id}", tags=["driver"], response_model=Driver)
async def get_driver_by_id(driver_id: int):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT * FROM driver WHERE driverId = %s", (driver_id,)
        )
        driver = db_connection.cursor.fetchone()
        if driver:
            return Driver(
                driverId=driver[0],
                driverRef=driver[1],
                number=driver[2],
                code=driver[3],
                forename=driver[4],
                surname=driver[5],
                dob=str(driver[6]),
                nationality=driver[7],
                url=driver[8],
            )
        else:
            return Response(status_code=404, content="Driver not found")

@router.get('/wins_by_constructor/{constructor_id}')
async def get_drivers_wins_by_constructor(constructor_id: int):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT DISTINCT(A.driverdriverId), A.forename, A.surname, B.poles FROM ( "\
            "SELECT driver.driverId AS driverdriverId, driver.forename AS forename, driver.surname AS Surname FROM driver " \
            "JOIN results ON Driver.driverId = results.driverId "\
            "WHERE results.constructorId = %s " \
            ") A LEFT JOIN " \
            "(SELECT driverId, COUNT(*) AS poles FROM results "\
            "WHERE results.constructorId = %s AND results.position = 1 "\
            "GROUP BY driverId) B " \
            "ON A.driverdriverId = B.driverId " \
            "ORDER BY A.driverdriverId;",(constructor_id, constructor_id)
        )
        drivers = db_connection.cursor.fetchall()
        drivers = [
            {
                "driverId": driver[0],
                "forename": driver[1],
                "surname": driver[2],
                "poles": driver[3]
            } for driver in drivers
        ]
        return drivers

@router.get('/{driver_id}/wins_by_year')
async def get_drivers_wins_by_year(driver_id: int):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT races.year, races.name, COUNT(*) AS poles FROM results " \
            "JOIN races ON races.raceid = results.raceid " \
            "WHERE results.driverid = %s " \
            "GROUP BY ROLLUP(races.year, races.name);", vars=(driver_id,)
        )
        wins = db_connection.cursor.fetchall()
        wins = [
            {
                'Ano': win[0],
                'Nome da Corrida': win[1],
                'Vitórias': win[2]
            }
            for win in wins
        ]
        return wins


@router.get('/{driver_id}/wins', tags=["driver"], response_model=dict)
async def get_driver_wins(driver_id: int):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT COUNT(*) FROM results WHERE driverId = %s AND position = '1'", (driver_id,)
        )
        count = db_connection.cursor.fetchone()
        return {'wins': count[0]}


@router.get('/{driver_id}/years', tags=["driver"], response_model=dict)
async def get_driver_years(driver_id: int):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT MIN(year), MAX(year) FROM results "
            "JOIN races ON results.raceId = races.raceId "
            "WHERE driverId = %s", (driver_id,)
        )
        years = db_connection.cursor.fetchone()
        return {'first_year': years[0], "last_year": years[1]}


@router.get("/", tags=["driver"], response_model=Union[List[Driver], dict])
async def get_all_drivers(count_only: bool = False):
    if count_only:
        with DBConnection() as db_connection:
            db_connection.cursor.execute("SELECT COUNT(*) FROM driver")
            count = db_connection.cursor.fetchone()
            return {'drivers_count': count[0]}
    with DBConnection() as db_connection:
        db_connection.cursor.execute("SELECT * FROM driver")
        drivers = db_connection.cursor.fetchall()
        return [
            Driver(
                driverId=driver[0],
                driverRef=driver[1],
                number=driver[2],
                code=driver[3],
                forename=driver[4],
                surname=driver[5],
                dob=str(driver[6]),
                nationality=driver[7],
                url=driver[8]
            )
            for driver in drivers
        ]


@router.post("/", tags=["driver"])
async def create_driver(driver: NewDriver):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "INSERT INTO driver "
            "VALUES (DEFAULT, %s, %s, %s, %s, %s, %s, %s, %s)",
            (driver.driverRef, driver.number, driver.code, driver.forename,
             driver.surname, driver.dob, driver.nationality, driver.url)
        )



