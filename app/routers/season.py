from fastapi import APIRouter

from dependencies import DBConnection

router = APIRouter()


@router.get("/count", tags=["season"], response_model=dict)
async def get_season_count():
    with DBConnection() as db_connection:
        db_connection.cursor.execute("SELECT COUNT(*) FROM seasons")
        count = db_connection.cursor.fetchone()
        return {'seasons_count': count[0]}
