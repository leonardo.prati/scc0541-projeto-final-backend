from fastapi import APIRouter
from dependencies import DBConnection

router = APIRouter()

@router.get("/", tags=["status"], response_model=dict)
async def list_and_count_status_by_query(constructor_id: int=None, driver_id: int=None):
    print(constructor_id, driver_id)
    if constructor_id:
        filter = "WHERE R.ConstructorId = %s"
        filter_args = (constructor_id,)
        return query_with_filter(filter, filter_args)
    elif driver_id:
        filter = "WHERE R.DriverId = %s"
        filter_args = (driver_id,)
        return query_with_filter(filter, filter_args)
    else:
        with DBConnection() as db_connection:
            db_connection.cursor.execute(
                "SELECT S.Status, COUNT(*) " \
                "FROM Status S " \
                "JOIN Results R ON R.StatusId = S.StatusId " \
                "GROUP BY S.StatusId, S.Status " \
                "ORDER BY S.StatusId;"
            )
            count = db_connection.cursor.fetchall()
            return(count)

def query_with_filter(filter: str, filter_args: tuple):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT S.Status, COUNT(*) " \
            "FROM Status S " \
            "JOIN Results R ON R.StatusId = S.StatusId " \
            + filter + \
            "GROUP BY S.StatusId, S.Status " \
            "ORDER BY S.StatusId;",
            (filter_args)
        )
        count = db_connection.cursor.fetchall()
        return(count)