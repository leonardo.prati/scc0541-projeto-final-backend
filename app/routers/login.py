from fastapi import APIRouter, Response

from dependencies import DBConnection
from models.user import UserInfo
from models.login import LoginInfo

router = APIRouter()

@router.post("/", tags=["login"], response_model=UserInfo)
async def login(login_info: LoginInfo):
    with DBConnection() as db_connection:
        db_connection.cursor.execute(
            "SELECT * FROM users WHERE login = %s AND password = MD5(%s)", (
                login_info.username, login_info.password)
        )
        user = db_connection.cursor.fetchone()
        if user:
            # Log the access in log_table
            insert = db_connection.cursor.execute(
                "INSERT INTO log_table (userId, accesstimestamp) VALUES (%s, NOW())", (user[0],)
            )
            print(insert, flush=True)
            return UserInfo(username=user[1], role=user[3], original_id=user[4])
        else:
            return Response(status_code=401, content="Invalid username or password")
