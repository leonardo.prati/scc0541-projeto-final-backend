from pydantic import BaseModel

class Constructor(BaseModel):
    constructorId: int
    constructorRef: str = None
    name: str = None
    nationality: str = None
    url: str = None


class NewConstructor(BaseModel):
    constructorRef: str = None
    name: str = None
    nationality: str = None
    url: str = None