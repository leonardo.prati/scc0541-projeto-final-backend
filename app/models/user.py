from pydantic import BaseModel

class UserInfo(BaseModel):
    username: str
    role: str
    original_id: int