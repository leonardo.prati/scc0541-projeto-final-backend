from pydantic import BaseModel

class Driver(BaseModel):
    driverId: int
    driverRef: str = None
    number: int = None
    code: str = None
    forename: str = None
    surname: str = None
    dob: str = None
    nationality: str = None
    url: str = None

class NewDriver(BaseModel):
    driverRef: str
    number: int = None
    code: str = None
    forename: str = None
    surname: str = None
    dob: str = None
    nationality: str = None
    url: str = None