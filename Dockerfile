FROM python:3.8-slim

RUN apt update && apt upgrade -y
RUN apt install -y curl

ENV POETRY_VERSION="1.1.4"
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/${POETRY_VERSION}/get-poetry.py | python3 -
RUN ln -s /root/.poetry/bin/poetry /usr/local/bin/poetry
COPY ./pyproject.toml /deploy/

WORKDIR /deploy
RUN poetry config virtualenvs.create false
RUN poetry install

COPY ./app /deploy/app
CMD python3 ./app/main.py